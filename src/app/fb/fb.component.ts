import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-fb',
  templateUrl: './fb.component.html',
  styleUrls: ['./fb.component.scss']
})
export class FbComponent implements OnInit {
  registerForm: FormGroup;
  model: any = {};

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createRegisterForm();
  }

  createRegisterForm() {
    this.registerForm = this.fb.group({
      gender: ['male'],
      username: ['', Validators.required],
      displayName: ['', Validators.required],
      dateOfBirth: [null, Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      postcode: ['', [
        Validators.required,
        Validators.pattern(
'^([A-Za-z]{1,2})([0-9]{1,2})(\\s{0,1})([0-9]{1,2})([A-Za-z]{2})$'
      )]],
      phone: ['', [
        Validators.required,
        Validators.pattern(
          '^(0{1})([1-9]{1})([0-9]{2,3})(\\s{0,1})([0-9]{6,8})$'
      )]],
      password: ['', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(8)
                ]],
      confirmPassword: ['', Validators.required]
    }, { validator: this.passwordMatchValidator});
  }

  passwordMatchValidator(fg: FormGroup) {
    return fg.get('password').value === fg.get('confirmPassword').value ? null : { 'mismatch' : true };
  }

  register() {
    console.log(this.registerForm.value);
  }

  cancel() {
    console.log('cancelled');
  }

}
