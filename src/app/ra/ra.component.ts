import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-ra',
  templateUrl: './ra.component.html',
  styleUrls: ['./ra.component.scss']
})
export class RaComponent implements OnInit {
  registerForm: FormGroup;
  model: any = {};


  constructor() { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', [
                                      Validators.required,
                                      Validators.minLength(4),
                                      Validators.maxLength(8)
                                    ]),
      confirmPassword: new FormControl('', Validators.required)
    }, this.passwordMatchValidator);
  }

passwordMatchValidator(fg: FormGroup) {
  return fg.get('password').value === fg.get('confirmPassword').value ? null : { 'mismatch' : true };
}

register() {
  console.log(this.registerForm.value);
}

cancel() {
  console.log('cancelled');
}

}
